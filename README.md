## Projet Flappy Bird

- Julio Cesar Tepixtle Hernández.

Le jeu réalisé est Flappy Bird.

# Instructions du jeu

- Pour commencer le jeu premièrement il suffit de clicker le bouton play.
Après, le jeu commence avec le flappy bird au milieu de l'écran.
Chaque fois que l'utilisateur touche l'écran ou la touche espace du clavier, l'oiseau va simuler le vol vers le haut.
Le but est de passer entre les pipes sans les toucher pour obtenir le score.
A la fin l'écran va montrer le score obtenu, un médaille par rapport au score et le score le plus haut obtenu dans l'histoire de tes parties. 
- L'utilisateur peut partager son score ou réinitialiser une nouvelle partie.

# Fonctionnalités
- 5 niveaux + niveau final où la vitesse augmente chaque niveau.
- Highscore du jeu, avec une étiquette qui fait savoir que c'est un nouveau highscore.
- Partage de score + médaille par rapport au score.
- Bonus pour toucher une monnaie

# Plateforme où le jeu a été utilisé.

- Le jeu a été testé sur un dispositif avec Android 11, modèle OPPO A54 avec l'aide de l'application Unity Remote 5.

# Problèmes

- Au moment de passer à l'écran final sur le dispositif mobile, si l'utilisateur touchait l'écran ou le bouton, il passé au menu en ignorant l'action du bouton. La solution a été de faire l'action de revenir au menu seulement si l'utilisateur touchait la partie haut de l'écran. Autrement dit, s'il touchait le bouton, l'action de passe à l'écran menu ne fonctionne pas.
- La monnaie n'apparait qu'une seul fois. J'ai essayé de cloner le game object après qu'il soit touché pour l'oiseau, et l'ajouter une nouvelle position. Pas réussi à cause de manque de temps.
- Parfois il y a un bug sur les pipes. J'ai essayé de le résoudre avec une condition, si la nouvelle position des pipes surpasse la taille de l'écran, ils doivent réapparaitre dedans l'écran. Je n'ai pas réussi parce que la taille normale de pipes surpasse l'écran alors j'ai eu des problèmes pour obtenir la nouvelle position exacte par rapport à l'écran, car il doivent être dedans mais pas complétement.

* Sound from Zapsplat.com

