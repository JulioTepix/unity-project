using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class playMusic : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // Jouer la chanson dans le menu et le continuer dans le jeu
        Scene scene = SceneManager.GetActiveScene();
        if (scene.name == "scene2-Menu" || scene.name == "scene3-Game")
        {
            DontDestroyOnLoad(gameObject);
        }else{
            Destroy(gameObject); 
        }
    }
}
