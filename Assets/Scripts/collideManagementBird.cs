using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class collideManagementBird : MonoBehaviour
{
    private AudioSource audiosource;
    public AudioClip died;
    private Vector2 speed;
    public AudioClip fly;
    private Vector3 rightTopCameraBorder;
    private Vector3 leftBottomCameraBorder;
    private Vector3 siz;
    // Start is called before the first frame update
    void Start()
    {
        audiosource = GetComponent<AudioSource>(); //Component de l'audio
        speed = new Vector2(0,3); //vitesse du flappy
        rightTopCameraBorder = Camera.main.ViewportToWorldPoint (new Vector3(1,1,0));
        leftBottomCameraBorder = Camera.main.ViewportToWorldPoint (new Vector3(0,0,0));
    }

    // Update is called once per frame
    void Update()
    {
        siz.x = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        siz.y = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;
        //Si l'utilisateur touche une fois l'écran...
        if (Input.touchCount > 0){
            Touch touch = Input.GetTouch(0);
            // Si l'utilisateur arrête de toucher..
            if(touch.phase == TouchPhase.Ended){
                GetComponent<Rigidbody2D>().velocity = speed; //ajouter la vitesse au flappy bird
                audiosource.PlayOneShot(fly, 0.5F); //Ajoute de son
            } 
        }
        //Si l'utilisateur tape l'espace...
        if (Input.GetKeyDown(KeyCode.Space)){
            GetComponent<Rigidbody2D>().velocity = speed;
            audiosource.PlayOneShot(fly, 0.5F);
        }

        // L'oiseau ne peut pas sortir de l'haut de l'écran
        if(transform.position.y > rightTopCameraBorder.y - (siz.y / 2))
            transform.position = new Vector3(transform.position.x, rightTopCameraBorder.y - (siz.y / 2) , transform.position.z);
        // Si l'oiseau sort du bas de l'écran, il est mort.
        if(transform.position.y < leftBottomCameraBorder.y)
            gameObject.AddComponent<endAction>();

    }

    void OnTriggerEnter2D(Collider2D collider) {
        //Si l'oiseau traverse les pipes, l'utilisateur obtient un point
        if(collider.name == "box1" || collider.name == "box2"){
            GameState.Instance.addScore(1);
        }else if(collider.name == "Bonus"){
            GameState.Instance.addScore(2);
        }else{
            //Si l'oiseau touche un obstacle, le jeu términe 
            audiosource.PlayOneShot(died, 0.5F);
            gameObject.AddComponent<endAction>();
        }
    }
}
