using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class levelsControl : MonoBehaviour
{
    private int level;

    // Start is called before the first frame update
    void Start()
    {
        level = 0; // Début dès le niveau 0
    }

    // Update is called once per frame
    void Update()
    {
        //Modifier la vitesse des obstacles par rapport au niveau du jeu
        switch (GameState.Instance.getScorePlayer())
        {
            case 5: level = 1;
            editSpeed(-1.1f);
                break;
            case 15: level = 2;
            editSpeed(-1.2f);
                break;
            case 25: level = 3;
            editSpeed(-1.3f);
                break;
            case 35: level = 4;
            editSpeed(-1.4f);
                break;
            case 45: level = 5;
            editSpeed(-1.5f);
                break;
            default:
                break;
        }
        if(GameState.Instance.getScorePlayer() > 55){
            GameObject.FindWithTag("level").GetComponent<Text>().text = "Final level"; 
        }else{
            GameObject.FindWithTag("level").GetComponent<Text>().text = "Level " + level;//Afficher le niveau
        }
    }

    void editSpeed(float speed){ //Modification de la vitesse
        GameObject.FindWithTag("pipes1").GetComponent<movePipes>().moreSpeed(speed);
        GameObject.FindWithTag("pipes2").GetComponent<movePipes>().moreSpeed(speed);
    }
}
