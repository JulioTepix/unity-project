using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class ShareScore : MonoBehaviour
{
    public Button share;
    // Start is called before the first frame update
    void Start()
    {
		//Si l'utilisateur touche le button pour partager, exécuter la fonction pour prendre une photo à l'écran
        if(share){
            share.onClick.AddListener(() => {StartCoroutine(TakeScreenshotAndShare()); });
        }
    }

    private IEnumerator TakeScreenshotAndShare()
    {
	    yield return new WaitForEndOfFrame();

	    Texture2D ss = new Texture2D( Screen.width, Screen.height, TextureFormat.RGB24, false );
	    ss.ReadPixels( new Rect( 0, 0, Screen.width, Screen.height ), 0, 0 );
	    ss.Apply();

	    string filePath = Path.Combine(Application.temporaryCachePath, "sharedimg.png");
	    File.WriteAllBytes(filePath, ss.EncodeToPNG());

	    // To avoid memory leaks
	    Destroy(ss);

	    new NativeShare().AddFile(filePath)// Au moment de prendre la photo, on peut choisir un reseau social pour la partager. 
		.SetSubject("Regarde mon score").SetText("Mon score en flappy bird").SetUrl("https://github.com/yasirkula/UnityNativeShare")
		.SetCallback((result, shareTarget) => Debug.Log( "Share result: " + result + ", selected app: " + shareTarget))
		.Share();

		// Share on WhatsApp only, if installed (Android only)
	    /*if( NativeShare.TargetExists( "com.whatsapp" ) )
	        new NativeShare().AddFile( filePath ).AddTarget( "com.whatsapp" ).Share();*/
    }
}
