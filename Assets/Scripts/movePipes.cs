using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movePipes : MonoBehaviour
{
    public Vector2 movement;
    public GameObject pipe1Up;
    public GameObject pipe1Down;
    public GameObject box;
    private Transform pipe1UpOriginalTransform;
    private Transform pipe1DownOriginalTransform;
    private Transform boxOriginalTransform;
    private Vector3 rightBottomCameraBorder;
    private Vector3 leftBottomCameraBorder;
    private Vector3 rightTopCameraBorder;
    private Vector3 siz;
    // Start is called before the first frame update
    void Start()
    {
        pipe1DownOriginalTransform = pipe1Down.transform;
        pipe1UpOriginalTransform = pipe1Up.transform;
        boxOriginalTransform = box.transform;
        leftBottomCameraBorder = Camera.main.ViewportToWorldPoint (new Vector3(0,0,0));
        rightBottomCameraBorder = Camera.main.ViewportToWorldPoint (new Vector3(1, 0, 0));
        rightTopCameraBorder = Camera.main.ViewportToWorldPoint (new Vector3(1,1,0));
    }

    // Update is called once per frame
    void Update()	
	{		
		pipe1Up.GetComponent<Rigidbody2D>().velocity = movement;	//	Déplacement	du	pipe haut	
		pipe1Down.GetComponent<Rigidbody2D>().velocity = movement;
        box.GetComponent<Rigidbody2D>().velocity = movement;
		siz.x = pipe1Up.GetComponent<SpriteRenderer> ().bounds.size.x;	//	Récuperation de	la	taille	d’un pipe		
		siz.y = pipe1Up.GetComponent<SpriteRenderer> ().bounds.size.y;	//	Suffisant car ils ont la même taille

		if (pipe1Up.transform.position.x < leftBottomCameraBorder.x - (siz.x / 2)) moveToRightPipe();

	}	
	
    void moveToRightPipe(){
        
		float randomY =	Random.Range(0.5f,3.5f) - 2;	//	Tirage aléatoire	d’un	décalage	en	Y	
		float posX = rightBottomCameraBorder.x + (siz.x / 2);	//	Calcul	du	X	du	bord droite	de	l’écran
		//	Calcul	du	nouvel	Y	en	reprenant	la	position	Y	d’origine	du	pipe,	ici	le	downPipe1	
		float posY = pipe1UpOriginalTransform.position.y + randomY;
		//	Création	du	vector3	contenant	la	nouvelle	position	
		Vector3 tmpPos = new Vector3 (posX,	posY, pipe1Up.transform.position.z);
		pipe1Up.transform.position = tmpPos;	
		//	Idem	pour	le	second	pipe	
		posY = pipe1DownOriginalTransform.position.y + randomY;
        
		tmpPos = new Vector3 (posX,posY, pipe1Down.transform.position.z);	
		pipe1Down.transform.position = tmpPos;
        //Idem pour le gameobject qui fait les points du jeu
        posY = boxOriginalTransform.position.y + randomY;	
		tmpPos = new Vector3 (posX,posY, box.transform.position.z);	
		box.transform.position = tmpPos;
	}
    /**
    * Méthode pour augmenter la vitesse de mouvement.
    */
    public void moreSpeed(float speed){
        movement.x = speed;
    }
}
