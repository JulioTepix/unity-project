using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighScore : MonoBehaviour
{
    public Text highscore;
    private int score;
    // Start is called before the first frame update
    void Start()
    {
        highscore.text = PlayerPrefs.GetInt("HighScores", 0).ToString(); // Montrer le score le plus haut obtenu dans le jeu.
        score = GameState.Instance.getScorePlayer(); // dernier score obtenu 
        getHighScore(); // appel de la méthode
    }

    public void getHighScore(){
        if (score > PlayerPrefs.GetInt("HighScores", 0)){
            PlayerPrefs.SetInt("HighScores", score);// Si le nouveau score est plus haut que celui qui est sauvegardé, il le remplace
            highscore.text = score.ToString(); //Afficher le nouveau score le plus haut
        }else{
            Destroy(GameObject.FindWithTag("newHighScore"));// montrer ce Gameobject seulement s'il y a un nouveau récord
        }

        if (score < 5){
            Destroy(GameObject.FindWithTag("Gold"));
            Destroy(GameObject.FindWithTag("Silver"));
            Destroy(GameObject.FindWithTag("Bronze"));
        }
        if (score < 15){ //Montrer les medailles pour chaque rang de points
            Destroy(GameObject.FindWithTag("Gold"));
            Destroy(GameObject.FindWithTag("Silver"));
        }else if (score < 30 && score >= 15){
            Destroy(GameObject.FindWithTag("Bronze"));
            Destroy(GameObject.FindWithTag("Gold"));
        }else{
            Destroy(GameObject.FindWithTag("Bronze"));
            Destroy(GameObject.FindWithTag("Silver"));
        }
    }
}
