using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveBk : MonoBehaviour
{
    private Vector3 siz;
    private Vector2 movement;
    private float positionRestartX;
    public GameObject bk1;
    public GameObject bk2;
    public GameObject bk3;
    private Vector3 leftBottomCameraBorder;
    // Start is called before the first frame update
    void Start()
    {
        leftBottomCameraBorder = Camera.main.ViewportToWorldPoint (new Vector3(0,0,0));
        movement = new Vector2(-2,0); //vitesse du background
        positionRestartX = bk3.transform.position.x; // position pour bouger l'arrière-plan
    }

    // Update is called once per frame
    void Update()
    {
        bk1.GetComponent<Rigidbody2D>().velocity = movement;
        bk2.GetComponent<Rigidbody2D>().velocity = movement;
        bk3.GetComponent<Rigidbody2D>().velocity = movement;												
        siz.x = bk1.GetComponent<SpriteRenderer> ().bounds.size.x;	
		siz.y = bk1.GetComponent<SpriteRenderer> ().bounds.size.y;

            // Si un image de l'arrière-plan sort de l'écran, sa nouvelle position sera de l'autre côté pour faire la simulation du boucle.
			if(bk1.transform.position.x < leftBottomCameraBorder.x - (siz.x / 2)){
				bk1.transform.position = new Vector3(positionRestartX,bk1.transform.position.y,bk1.transform.position.z);	
			}
            if(bk2.transform.position.x < leftBottomCameraBorder.x - (siz.x / 2)){
				bk2.transform.position = new Vector3(positionRestartX,bk2.transform.position.y,bk2.transform.position.z);	
			}
            if(bk3.transform.position.x < leftBottomCameraBorder.x - (siz.x / 2)){
				bk3.transform.position = new Vector3(positionRestartX,bk3.transform.position.y,bk3.transform.position.z);	
			}
    }
}
