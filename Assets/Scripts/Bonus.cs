using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bonus : MonoBehaviour
{
    private Vector2 movement;
    private Transform bonusOriginalTransform;
    private AudioSource audiosource;
    public AudioClip coin;
    
    // Start is called before the first frame update
    void Start()
    {
		bonusOriginalTransform = gameObject.transform;
        audiosource = GetComponent<AudioSource>();
        movement = GameObject.FindWithTag("pipes1").GetComponent<movePipes>().movement;//ajouter la même vitesse que les pipes
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<Rigidbody2D>().velocity = movement;
        transform.Rotate(0, -1, 0);
    }

    void OnTriggerEnter2D(Collider2D collider) {
        //Si l'oiseau touche la monnaie 
        if(collider.name == "Bird"){
            audiosource.PlayOneShot(coin, 0.7F);
            gameObject.AddComponent<endActionBonus>();
        }
    }
}
