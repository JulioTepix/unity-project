using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class endAction : MonoBehaviour
{
    private Vector3 rotate;
    private Vector3 siz;
    private Vector3 leftBottomCameraBorder;

    // Start is called before the first frame update
    void Start()
    {
        leftBottomCameraBorder = Camera.main.ViewportToWorldPoint (new Vector3(0,0,0));
    }

    // Update is called once per frame
    void Update()
    {
        siz.x = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        siz.y = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;
        Destroy(GetComponent<collideManagementBird>());// Détruire le code qui gére le collider afin de ne pas les produire après la mort de l'oiseau. En plus, pour quitter la fonction de toucher pour augmenter la vitesse de l'oiseau.
        transform.Rotate(0, 0, -1);//Rotation de l'oiseau
        if(transform.position.y < leftBottomCameraBorder.y){
            SceneManager.LoadScene("scene4-End"); // Un fois l'oiseau sort de l'écran, on passe à la scène final.
        }
    }
}
