using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class endActionBonus : MonoBehaviour
{
    private Vector3 rotate;
    private Vector3 siz;
    private Vector3 leftBottomCameraBorder;
    private Vector2 down;


    // Start is called before the first frame update
    void Start()
    {
        leftBottomCameraBorder = Camera.main.ViewportToWorldPoint (new Vector3(0,0,0));
        down = new Vector2(0, -3);
    }

    // Update is called once per frame
    void Update()
    {
        if(transform.position.y < leftBottomCameraBorder.y){
            Destroy(gameObject); // une fois la monnaie sort de l'écran, elle est détriute
        }else{
            Destroy(GetComponent<Bonus>());
            gameObject.GetComponent<Rigidbody2D>().velocity = down;//La monnaie va tomber
            transform.Rotate(0, -3, 0); //Rotation de la monnaie
        }
    }
}
