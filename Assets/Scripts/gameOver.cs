using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class gameOver : MonoBehaviour
{
    private Camera camara;
    private Vector3 position;
    // Start is called before the first frame update
    void Start()
    {
        camara = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0){
            Touch touch = Input.GetTouch(0); //Obtenir le touch
            position = camara.ScreenToWorldPoint(touch.position); //Obtenir la position du touch par rapport à l'écran

            if(position.y > -1) //Si la position du touch est plus que -1, on passe au menu
                SceneManager.LoadScene("scene2-Menu");
        }

        if(Input.GetKeyDown(KeyCode.Space)){ //si on tape sur l'espace, on passe au menu.
            SceneManager.LoadScene("scene2-Menu");
        }
    }

}
