using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;	
using UnityEngine.SceneManagement;

public class GameState : MonoBehaviour
{
    
    private int scorePlayer;
    public static GameState Instance { get; private set; } //Créer un instance unique
    private void Awake() 
    { 
    // If there is an instance, and it's not me, delete myself.
    
        if (Instance != null && Instance != this) 
        { 
            Destroy(this); 
        } 
        else 
        { 
            Instance = this;
        } 
    }

    // Update is called once per frame
    void Update()
    {
        Scene scene = SceneManager.GetActiveScene();
        if (scene.name == "scene3-Game")// Si on passe de la scène 3 à la 4, il ne faut pas détruire le gameobject.
        {
            DontDestroyOnLoad(gameObject);
        }else{
            Destroy(gameObject);// Sinon il faut le détruire
        }
        GameObject.FindWithTag("score").GetComponent<Text>().text = "" + scorePlayer; //ajoute du score au component text pour l'afficher.
    }

    public void addScore(int toAdd){
        scorePlayer += toAdd; //ajoute du score
	}	
	
    public int getScorePlayer(){
        return scorePlayer;	// Obtenir le score
	}
}
